%global nsver 1.0

%global commit 8a7a4a39c63000ca5f41ddfce8ce80d792f6b9cc
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:           graphene
Version:        1.5.1~git.7.%{shortcommit}
Release:        1%{?dist}
Summary:        A thin layer of types for graphic libraries

License:        MIT
URL:            https://github.com/ebassi/graphene
Source0:        %{url}/archive/%{commit}/%{name}-%{shortcommit}.tar.gz
# https://github.com/ebassi/graphene/pull/78
Patch0001:      0001-build-install-tests-into-libexecdir.patch

BuildRequires:  gcc
BuildRequires:  meson
BuildRequires:  pkgconfig(gobject-2.0)

%description
Graphene provides a small set of mathematical types needed to implement
graphic libraries that deal with 2D and 3D transformations and projections.

%package devel
Summary:        Development libraries and header files for %{name}
Requires:       %{name}%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}

%description devel
%{summary}.

%package tests
Summary:        Tests for %{name}
Requires:       %{name}%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}

%description tests
The %{name}-tests package contains tests that can be used to verify
the functionality of the installed %{name} package.

%prep
%autosetup -n %{name}-%{commit} -p1

%build
# https://github.com/mesonbuild/meson/issues/1085
export LC_ALL=C.UTF-8
%meson
%meson_build

%install
%meson_install

%check
%meson_test

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%license LICENSE
%doc README.md CONTRIBUTING.md
%{_libdir}/libgraphene-%{nsver}.so.*
%dir %{_libdir}/girepository-1.0
%{_libdir}/girepository-1.0/Graphene-1.0.typelib

%files devel
%{_libdir}/libgraphene-%{nsver}.so
%{_libdir}/graphene-1.0/
%{_includedir}/graphene-%{nsver}/
%{_libdir}/pkgconfig/graphene-%{nsver}.pc
%{_libdir}/pkgconfig/graphene-gobject-%{nsver}.pc
%dir %{_datadir}/gir-1.0
%{_datadir}/gir-1.0/Graphene-%{nsver}.gir

%files tests
%dir %{_libexecdir}/installed-tests
%{_libexecdir}/installed-tests/graphene-%{nsver}/

%changelog
